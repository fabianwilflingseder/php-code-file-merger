﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PHP_file_merger
{
    public static class ListExtensionMethods
    {
        public static string ToFileContent(this List<string> list) => String.Join("\r\n", list);
    }
}
