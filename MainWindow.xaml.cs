﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PHP_file_merger
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow() => InitializeComponent();

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void menuItemNew_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog fileOpenDialog = new OpenFileDialog();
            fileOpenDialog.DefaultExt = "php";
            fileOpenDialog.Title = "PHP index Datei öffnen";
            fileOpenDialog.Filter = "PHP files (*.php)|*.php|All files (*.*)|*.*";
            fileOpenDialog.ShowDialog();
            string filename = fileOpenDialog.FileName;
            PHPFileLinkingParser parser = new PHPFileLinkingParser(filename);
            List<string> result = parser.DoParse();
            txtResult.Text = result.ToFileContent();
        }

        private void menuItemSave_Click(object sender, RoutedEventArgs e)
        {
            //Save file
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //Set php highlighting
        }

        private void menuItemAbout_Click(object sender, RoutedEventArgs e)
        {
            AboutWindow windows = new AboutWindow();
            windows.ShowDialog();
        }
    }
}
