﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Text.RegularExpressions;

namespace PHP_file_merger
{
    public class PHPFileLinkingParser
    {
        public event EventHandler<FileParsedEventArgs> FileParsedEvent;
        public event EventHandler<ResultContentAppendedEventArgs> ResultContentAppendedEvent;
        
        public string EntryPointPhpFileName { get; set; }

        private List<string> ResultContent { get; set; } = new List<string>();

        public PHPFileLinkingParser(string entryPointPhpFileName) => EntryPointPhpFileName = entryPointPhpFileName;

        public List<string> DoParse()
        {
            ParseFile(EntryPointPhpFileName);
            return ResultContent;
        }

        public void ParseFile(string filename)
        {
            string[] input = File.ReadAllLines(filename);
            string basePath = Path.GetDirectoryName(filename);
            foreach (string line in input)
            {
                string linkingFile = GetFilenameOfLinkingIfExists(basePath, line);
                if (linkingFile != null)
                {
                    ParseFile(linkingFile);
                }
                else
                {
                    ResultContent.Add(line);
                }
            }
        }

        public static string GetFilenameOfLinkingIfExists(string baseFilePath, string codeLine)
        {
            const string regexStr1 = "(require|require_once|include|include_once)\\s*\\(?\\s*(.*)\\s*\\)?";
            const string regexStr2 = "(\"|')/?(.*)\\1";
            string regexStr3 = $"dirname\\s*\\(\\s*__FILE__\\s*\\.\\s*{regexStr2}\\)";

            var regex1 = new Regex(regexStr1);
            var match1 = regex1.Match(codeLine);
            string filename = null;
            if (match1.Success)
            {
                string param = match1.Groups[2].Value;
                var regex2 = new Regex(regexStr2);
                var match2 = regex2.Match(param);
                if (match2.Success)
                {
                    filename = match2.Groups[2].Value;
                }
                else
                {
                    var regex3 = new Regex(regexStr3);
                    var match3 = regex3.Match(match2.Groups[2].Value);
                    if (match3.Success)
                    {
                        filename = match3.Groups[2].Value;
                    }
                }
            }
            return $"{baseFilePath}{filename}";
        }

        public enum PHPLinkingType
        {
            REQUIRE, INCLUDE, REQUIRE_ONCE, INCLUDE_ONCE
        }
    }
}
